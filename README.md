MFRC522-python3
==============

The code is intended for the use of the RFID-RC522 (or Mifare) Chip with a Raspberry Pi 3.


# Hint and thanks to the original authors:
This is a copy of https://github.com/mxgxw/MFRC522-python.git from Mario Gómez and others who implemented it for python2.
The only work of adaption for python3 from my side so far is the correct use of the print function with round brackets for python3.
The original code is a Python port of the example code for the NFC module MF522-AN.

## Installation instructions
Please type the following commands in a terminal within your Raspberry Pi 3
```	
	sudo apt-get install python3-dev

	git clone https://github.com/lthiery/SPI-Py.git

	cd SPI-Py
	
	sudo python3 setup.py install

	cd ..
	
	git clone https://gitlab.com/marc-fernandes/MFRC522-python3.git	
```

Now you can test everything with the following example:

```
	cd MFRC522-python3
	python3 Read.py

```

## Examples
The repository includes all needed examples showing how to read, write, and dump data from a chip. 

## Pins and Setup

| Pin RFID-RC522 | Hardware Pin # - Raspberry Pi | Pin BCM-Number / Name   |
|------|-------|------------|
| SDA  | 24    | GPIO8      |
| SCK  | 23    | GPIO11     |
| MOSI | 19    | GPIO10     |
| MISO | 21    | GPIO9      |
| IRQ  | None  | None       |
| GND  | Any   | Any Ground |
| RST  | 22    | GPIO25     |
| 3.3V | 1     | 3V3        |

![Hardware Setup](rfid_setup.png)

## Usage
Import the class by importing MFRC522 in the top of your script. For more info see the examples.
